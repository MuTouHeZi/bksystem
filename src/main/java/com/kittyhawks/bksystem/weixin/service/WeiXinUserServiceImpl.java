package com.kittyhawks.bksystem.weixin.service;

import com.kittyhawks.bksystem.weixin.util.WeiXinFinalValue;
import com.kittyhawks.bksystem.weixin.dto.WeiXinAuthorizeDTO;
import com.kittyhawks.bksystem.weixin.dto.WeiXinUserInfoDTO;
import com.kittyhawks.bksystem.weixin.exception.WxErrorException;
import com.kittyhawks.bksystem.weixin.kit.WeiXinCheckKit;
import com.kittyhawks.bksystem.weixin.service.WeiXinUserService;
import com.kittyhawks.bksystem.weixin.service.WeixinBaseService;
import com.kittyhawks.bksystem.weixin.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WeiXinUserServiceImpl implements WeiXinUserService {

	@Autowired
	private WeixinBaseService weixinBaseService;
	@Override
	public WeiXinAuthorizeDTO getAuthorizeAccessTokenService(String code)
			throws WxErrorException {
		String url= WeiXinFinalValue.WX_AUTHORIZE_ACCESSTOKEN;
		url=url.replace("APPID", WeiXinFinalValue.APPID).replace("SECRET",//
				WeiXinFinalValue.APPSECRET).replace("CODE", code);
		String reMsgContent=weixinBaseService.get(url, null);
		return WeiXinCheckKit.checkRequestSucc(reMsgContent)?(WeiXinAuthorizeDTO)//
				JsonUtil.getInstance().json2obj(reMsgContent, WeiXinAuthorizeDTO.class):null;
	}

	@Override
	public WeiXinUserInfoDTO getAuthorizeUserInfo(
			WeiXinAuthorizeDTO weiXinAuthorizeDTO) throws WxErrorException {
			String  url=WeiXinFinalValue.WX_AUTHORIZE_INFO;
			url=url.replace("ACCESS_TOKEN", weiXinAuthorizeDTO.getAccess_token())//
					.replace("OPENID", weiXinAuthorizeDTO.getOpenid());
			String reMsgContent=weixinBaseService.get(url,null);
			return WeiXinCheckKit.checkRequestSucc(reMsgContent)?(WeiXinUserInfoDTO)//
					JsonUtil.getInstance().json2obj(reMsgContent, WeiXinUserInfoDTO.class):null;
	}
}
