package com.kittyhawks.bksystem.util;

import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @Author: liangbin
 * @Date: 2018/3/24 1:28
 */
@Component
public class JSONResult implements Serializable{
    private int status;
    private String message;
    private Object data;

    public JSONResult() {
        this.data = "";
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
