package com.kittyhawks.bksystem.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *  @Author Tony
 *
 */
public class DateUtil {

    /**
     * string转date
     * @param date
     * @return
     */
    public static Date String2Date(String date) {
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(false);
        try {
            return sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        throw new IllegalArgumentException("Illegal Date Format");
    }

    /**
     * string转sqlDate
     * @param datestring
     * @return
     */
    public static java.sql.Date String2SqlDate(String datestring) {
        return java.sql.Date.valueOf(datestring);
    }


    /**
     * 日期转String
     * @param date
     * @return
     */
    public static String Date2String(Date date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(date);
        return dateString;
    }

    /**
     * 日期转时间
     * @param date
     * @return
     */
    public static String Date2string(Date date){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy,MM,dd");
        String dateString = formatter.format(date);
        return dateString;
    }

    /**
     * String转week(int型)
     * @param datetime
     * @return
     */
    public static int date2Week(String datetime) {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        Date datet = null;
        try {
            datet = f.parse(datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date2Week(datet);

    }

    /**
     * 日期转week
     * @param datetime
     * @return
     */
    public static int date2Week(Date datetime) {
        Calendar cal = Calendar.getInstance(); // 获得一个日历
            cal.setTime(datetime);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        return w;//0星期日，星期一。。。
    }


    public static String date2Weekday(Date datetime) {
        String[] weekDays = { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
        int w = date2Week(datetime);
        return weekDays[w];
    }
}
