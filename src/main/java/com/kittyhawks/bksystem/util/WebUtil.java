package com.kittyhawks.bksystem.util;


import com.kittyhawks.bksystem.weixin.util.WeiXinFinalValue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * session
 */
public class WebUtil {

    private static Logger logger = LogManager.getLogger();

    /** 保存当前用户 */
    public static final void saveCurrentUser(HttpServletRequest request, Object user) {
        setSession(request,WeiXinFinalValue.WX_SESSION_USER, user);
    }


    /** 获取当前用户 */
    public static final Object getCurrentUser(HttpSession session) {
        try {
            if (null != session) {
                return session.getAttribute(WeiXinFinalValue.WX_SESSION_USER);
            }
        } catch (Exception e) {
            logger.error(e);
        }
        return null;
    }

    /** 移除当前用户 */
    public static final void removeCurrentUser(HttpSession session) {
        session.removeAttribute(WeiXinFinalValue.WX_SESSION_USER);
    }


    public static final void setSession(HttpServletRequest request, String key, Object value) {
        HttpSession session = request.getSession();
        if (null != session) {
            session.setAttribute(key, value);
        }
    }
}

