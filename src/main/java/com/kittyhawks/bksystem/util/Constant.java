package com.kittyhawks.bksystem.util;

public interface Constant {

    static final int SUCCESS_CODE = 1;
    static final int FAILURE_CODE = 0;
}
