package com.kittyhawks.bksystem.controller;

import com.kittyhawks.bksystem.entity.Fee;
import com.kittyhawks.bksystem.service.FinanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/finance")
public class FinanceController {
    @Autowired
    FinanceService financeService;

    @PostMapping("/selectFeeById")
    public Fee selectFeeById(String id){
        return financeService.selectById(id);
    }

    @PostMapping("/selectFeesByParams")
    public List<Fee> selectFeesByParams(Fee fee){
       try {
         List<Fee> list = financeService.selectFeesByParams(fee);
         return list;
       }catch(Exception e){
           e.printStackTrace();
           return null;
       }
    }

    @GetMapping("/deleteFeeById")
    public String deleteFeeById(String id){
        try {
            int result = financeService.deleteById(id);
            if (result == 0) {
                return "FALSE";
            } else {
                return "TRUE";
            }
        }catch(Exception e){
            return "FALSE";
        }
    }

    @PostMapping("/insertFee")
    public String insertFee(Fee fee){
        try {
            fee.setId(UUID.randomUUID().toString().replace("-",""));
            financeService.insert(fee);
            return "SUCCESS";
        }catch (Exception e){
            return "FALSE";
        }
    }

    @PostMapping("/updateFee")
    public String updateFee(Fee fee){
        try{
            return financeService.updateFee(fee)+"";
        }catch(Exception e){
            e.printStackTrace();
            return "FALSE";
        }
    }

    @RequestMapping("/getfinishorder")
    public String getOrders() {
        String result = financeService.getFinishOrder();
        return result;
    }
}
