package com.kittyhawks.bksystem.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @Author Tony
 * 用于展示前台界面
 */
@Controller
public class FrontController {

    @GetMapping("/index")
    public String showIndex(){
        return "index";
    }

    @GetMapping("/index/news")
    public String showNews() { return "news";}

    @GetMapping("/phr/order")
    public String phrOrderList(){
        return "phrOrderList";
    }
    @GetMapping("/user/myOrderList")
    public String myOrderList(){
        return "userOrderList";
    }
    @GetMapping("/user/myOrderDetail")
    public String myOrderDetail(){
        return "userOrderDetail";
    }
    @GetMapping("/user/order")
    public String userPage(){
        return "confirmOrder";
    }

    @GetMapping("/phr/order/detail")
    public String phrOrderDetail(){
        return "phrOrderDetail";
    }

    @GetMapping("/user/order/detail")
    public String userOrderDetail(){
        return "usrOrderDetail";
    }

    @GetMapping("/user/order/add")
    public String confirmOrder(){
        return "confirmOrder";
    }
}
