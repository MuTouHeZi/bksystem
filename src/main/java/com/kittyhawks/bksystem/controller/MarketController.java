package com.kittyhawks.bksystem.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kittyhawks.bksystem.entity.BookingInfo;
import com.kittyhawks.bksystem.entity.Order;
import com.kittyhawks.bksystem.service.MarketService;
import com.kittyhawks.bksystem.util.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by dell on 2018/3/24.
 */
@RestController()
@RequestMapping("/marketManager")
public class MarketController {

    @Autowired
    MarketService marketService;
    @Autowired
    JSONResult jsonResult;

    ObjectMapper objectMapper = new ObjectMapper();



    @GetMapping("/bookingInfo")
    public String getBookingInfo(){
        List<BookingInfo> bookingInfos = marketService.getOrderList();
        String data = null;
        try {
            data = objectMapper.writeValueAsString(bookingInfos);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return data;
    }
//    @GetMapping("/bookingInfo/{id}")
//    public String getOrderDetail(@PathVariable("id") String id){
//        Order order = marketService.getOrderDetail(id);
//        String data = null;
//        try {
//            data = objectMapper.writeValueAsString(order);
////            data = data.substring(1,data.length()-1);
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }
//        return data;
//    }

    @GetMapping("/collegeRatio")
    public String getCollegeRatio(){
        List<Map> colRatio = marketService.getCollegeRatio();
        String data = null;
        try {
                data = objectMapper.writeValueAsString(colRatio);
                data = data.substring(1,data.length()-1);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
//        jsonResult.setData(data);
        return data;
    }
    @GetMapping("/orderNumOfMonth")
    public String getOrderNumOfMonth(){
        List<Map> orderNum = marketService.getOrderNumOfMonth();
        String data = null;

        try {
            data = objectMapper.writeValueAsString(orderNum);
            data = data.substring(1,data.length()-1);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

//        jsonResult.setData(data);

        return data;
    }
    @GetMapping("/orderNumOfDay")
    public String getOrderNumOfDay(){
        List<Map> orderNum = marketService.getOrderNumOfDay();
        String data = null;

        try {
            data = objectMapper.writeValueAsString(orderNum);
            data = data.substring(1,data.length()-1);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

//        jsonResult.setData(data);
        return data;
    }

    @GetMapping("/orderNumOfBTime")
    public String getOrderNumOfBTime(){
        List<Map> orderNum = marketService.getOrderNumOfBTime();
        String data = null;

        try {
            data = objectMapper.writeValueAsString(orderNum);
            data = data.substring(1,data.length()-1);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

//        jsonResult.setData(data);
        return data;
    }

    @GetMapping("/orderNumOfDayInMonth")
    public String getOrderNumOfDayInMonth(){
        List<Map> orderNum = marketService.getOrderNumOfDayInMonth();
        String data = null;

        try {
            data = objectMapper.writeValueAsString(orderNum);
            data = data.substring(1,data.length()-1);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

//        jsonResult.setData(data);
        return data;
    }

}
