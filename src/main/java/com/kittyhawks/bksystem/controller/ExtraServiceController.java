package com.kittyhawks.bksystem.controller;

import com.kittyhawks.bksystem.entity.ExtraService;
import com.kittyhawks.bksystem.entity.Premium;
import com.kittyhawks.bksystem.service.ExtraserviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class ExtraServiceController {
    @Autowired
    ExtraserviceService service;

    /**
     * 查询所有衣服
     *
     * @return
     */
    @RequestMapping(value = "/select", method = RequestMethod.POST)
    @ResponseBody
    public List<ExtraService> findAllClothes() {
        return service.findClothes();
    }

    /**
     * 查询所有服务
     *
     * @return
     */
    @RequestMapping(value = "/selectservice", method = RequestMethod.POST)
    @ResponseBody
    public List<ExtraService> findAllService() {
        return service.findService();
    }

    /**
     * 条件查询所有衣服
     *
     * @param extraService
     * @return
     */
    @RequestMapping(value = "/selectbycondition", method = RequestMethod.POST)
    @ResponseBody
    public List<ExtraService> findClothesByCondition(@RequestBody(required = false) ExtraService extraService) {
        return service.findClothesByCondition(extraService.getSize(), extraService.getName());
    }

    /**
     * 条件查询服务
     *
     * @param extraService
     * @return
     */
    @RequestMapping(value = "/selectservicebycondition", method = RequestMethod.POST)
    @ResponseBody
    public List<ExtraService> findServiceByCondition(@RequestBody(required = false) ExtraService extraService) {
        return service.findServiceByCondition(extraService.getName());
    }

    /**
     * 添加衣服
     *
     * @param extraService
     * @return
     */
    @RequestMapping(value = "/addclothes", method = RequestMethod.POST)
    @ResponseBody
    public String addClothes(@RequestBody(required = false) ExtraService extraService) {
        try {
            service.insertClothes(extraService);
            return "success";
        } catch (Exception e) {
            return "fail";
        }
    }

    /**
     * 添加服务
     *
     * @param extraService
     * @return
     */
    @RequestMapping(value = "/addservice", method = RequestMethod.POST)
    @ResponseBody
    public String addService(@RequestBody(required = false) ExtraService extraService) {
        try {
            service.insertService(extraService);
            return "success";
        } catch (Exception e) {
            return "fail";
        }
    }

    /**
     * 修改衣服
     *
     * @param extraService
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @ResponseBody
    public String updateClothes(@RequestBody ExtraService extraService) {
        try {
            if ("".equals(extraService.getDamageAmount())) {
                extraService.setDamageAmount(0);
            }
            if (extraService.getDamageAmount() <= extraService.getAmount()) {
                service.ClothesInfo(extraService);
                return "success";
            } else {
                return "fail";
            }
        } catch (Exception e) {
            return "fail";
        }
    }

    /**
     * 修改服务
     *
     * @param extraService
     * @return
     */
    @RequestMapping(value = "/editservice", method = RequestMethod.POST)
    @ResponseBody
    public String updateService(@RequestBody ExtraService extraService) {
        try {
            System.out.println(extraService.getName());
            service.setServiceInfo(extraService);
            return "success";
        } catch (Exception e) {
            return "fail";
        }
    }

    /**
     * 添加清洗衣服数量
     *
     * @param extraService
     * @return
     */
    @RequestMapping(value = "/clean", method = RequestMethod.POST)
    @ResponseBody
    public String cleanClothes(@RequestBody ExtraService extraService) {
        try {
            if ("".equals(extraService.getCleaningAmount())) {
                extraService.setCleaningAmount(0);
            }
            if (extraService.getCleaningAmount() <= extraService.getAmount()) {
                service.CleaningClothes(extraService);
                return "success";
            } else {
                return "fail";
            }
        } catch (Exception e) {
            return "fail";
        }
    }

    /**
     * 减少清洗衣服数量
     *
     * @param extraService
     * @return
     */
    @RequestMapping(value = "/finishclean", method = RequestMethod.POST)
    @ResponseBody
    public String finishClean(@RequestBody ExtraService extraService) {
        try {
            if ("".equals(extraService.getCleaningAmount())) {
                extraService.setCleaningAmount(0);
            }
            service.finishCleaning(extraService);
            return "success";
        } catch (Exception e) {
            return "fail";
        }
    }

    /**
     * 设置服务可用
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/setserviceuseable")
    @ResponseBody
    public String setServiceUseable(Integer id) {
        try {
            service.setServiceUseable(id);
            return "success";
        } catch (Exception e) {
            return "fail";
        }
    }

    /**
     * 设置服务不可用
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "/setserviceunuseable")
    @ResponseBody
    public String setServiceUnUseable(Integer id) {
        try {
            service.setServiceUnUseable(id);
            return "success";
        } catch (Exception e) {
            return "fail";
        }
    }

    /**
     * 上传图片
     *
     * @param file
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/uploadimg", method = RequestMethod.POST)
    @ResponseBody
    public String uploadimg(MultipartFile file) throws Exception {
        File file2 = new File("E:/bksystem/images/");
        //如果文件夹不存在则创建
        if (!file2.exists() && !file2.isDirectory()) {
            file2.mkdirs();
        }
        String filename = file.getOriginalFilename();
        SimpleDateFormat sd = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String newFilename = sd.format(new Date()) + filename.substring(filename.lastIndexOf("."));
        file.transferTo(new File("E:/bksystem/images/" + newFilename));
        return "E:/bksystem/images/" + newFilename;

//            File file2 =new File("/home/rex");
//            //如果文件夹不存在则创建
//            if  (!file2 .exists()  && !file2 .isDirectory())
//            {
//                file2.mkdirs();
//            }
//            String filename = file.getOriginalFilename();
//            SimpleDateFormat sd = new SimpleDateFormat("yyyyMMddHHmmssSSS");
//            String newFilename =sd.format(new Date()) + filename.substring(filename.lastIndexOf("."));
//            file.transferTo(new File("/home/rex" + newFilename));
//            return "/home/rex"+newFilename;
    }

//    @RequestMapping("useClothes")
//    @ResponseBody
//    public String useClothes() {
//        List<Premium> premiums = new ArrayList<>();
//        Premium premium1 = new Premium();
//        premium1.setAmount(20);
//        premium1.setExtra_id(4);
//        Premium premium2 = new Premium();
//        premium2.setAmount(20);
//        premium2.setExtra_id(68);
//        premiums.add(premium1);
//        premiums.add(premium2);
//        int i = service.useClothes(premiums);
//        if(i==0){
//        return "success";}
//        else{
//            return "fail";
//        }
//    }
}
