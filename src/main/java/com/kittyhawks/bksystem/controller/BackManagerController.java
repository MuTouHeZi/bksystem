package com.kittyhawks.bksystem.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kittyhawks.bksystem.dao.AdminMapper;
import com.kittyhawks.bksystem.entity.Admin;
import com.kittyhawks.bksystem.entity.User;
import com.kittyhawks.bksystem.service.BackManagerService;
import com.kittyhawks.bksystem.service.FeetypeService;
import com.kittyhawks.bksystem.service.OrderService;
import com.kittyhawks.bksystem.util.JSONResult;
import com.kittyhawks.bksystem.util.WebUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kittyhawks.bksystem.util.JSONUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static com.kittyhawks.bksystem.util.Constant.FAILURE_CODE;
import static com.kittyhawks.bksystem.util.Constant.SUCCESS_CODE;


@RestController
@RequestMapping("/backsys")
public class BackManagerController {
    @Autowired
    BackManagerService backManagerService;
    @Autowired
    AdminMapper adminMapper;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    JSONResult jsonResult;
    @Autowired
    FeetypeService feetypeService;
    @Autowired
    OrderService orderService;

    /**
     * @param request
     * @param response
     * @return
     * @Author:liangbin 张瀚
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public JSONResult login(HttpServletRequest request,
                            HttpServletResponse response) {
        String userName = request.getParameter("username");
        String password = request.getParameter("password");
        Admin admin = backManagerService.login(userName, password);
        if (admin!=null&&admin.getStatus()==0){
            String data = null;
            try {
                data = objectMapper.writeValueAsString(admin);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            jsonResult.setStatus(SUCCESS_CODE);
            jsonResult.setMessage("登陆成功");
            jsonResult.setData(data);
            HttpSession session = request.getSession();
            session.setAttribute("admin",jsonResult);
            WebUtil.saveCurrentUser(request, admin);
        }
        else {
            jsonResult.setStatus(FAILURE_CODE);
            jsonResult.setMessage("密码错误或账户异常");
        }
        return jsonResult;
    }


    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public JSONResult admin(HttpSession httpSession) throws JsonProcessingException {
        return (JSONResult) httpSession.getAttribute("admin");
    }

    @RequestMapping("/deleteorder")
    public String deleteOrder(@RequestParam("orderid") String orderid){
        orderService.cancelOrderByUser(orderid);
        return "SUCCESS";
    }

    @RequestMapping("/getallfees")
    public String getAllFees(){
        return feetypeService.getAllFees();
    }

    @RequestMapping("/getdetail")
    public String getDetail(@RequestParam("orderid") String orderid){
        return backManagerService.getDetailById(orderid);
    }

    @RequestMapping(value = "/updateorder",method = RequestMethod.POST)
    public String updateOrder(@RequestParam("orderid") String orderid,
                              @RequestParam("bookday") String bookday,
                              @RequestParam("booktime") String booktime,
                              @RequestParam("price") String price,
                              @RequestParam("status") String status,
                              @RequestParam("message") String message,
                              @RequestParam("total") String total) {
        return backManagerService.updateOrder(orderid,  bookday,  booktime,  price,  status,  message, total);
    }
}
