package com.kittyhawks.bksystem.controller;


import com.kittyhawks.bksystem.service.CommentService;
import com.kittyhawks.bksystem.util.JSONResult;
import com.kittyhawks.bksystem.util.WebUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpSession;
import java.util.UUID;

/**
 * Author： Tony
 */

@RestController
public class CommentController {

    @Autowired
    private CommentService commentService;

    @ApiOperation(value = "用户添加评论")
    public JSONResult addComment(@RequestParam("value") String value,
                                 @RequestParam("news_id") String news_id,
                                 HttpSession session){
        String userId = (String) WebUtil.getCurrentUser(session);
        String id = UUID.randomUUID().toString().replace("-","");
        return commentService.addComment(id,userId,news_id,value);
    }


    @ApiOperation(value = "点赞")
    @GetMapping("/like")
    public JSONResult likeNews(@RequestParam("id") String id){return commentService.likeComment(id);}

}
