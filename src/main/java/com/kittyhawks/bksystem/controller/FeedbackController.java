package com.kittyhawks.bksystem.controller;

import com.kittyhawks.bksystem.service.FeedbackService;
import com.kittyhawks.bksystem.util.JSONResult;
import com.kittyhawks.bksystem.util.WebUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpSession;
import java.util.UUID;

@Controller
public class FeedbackController {

    @Autowired
    private FeedbackService feedbackService;

    @GetMapping("/feedback")
    public String showFeed(){
        return "feedback";
    }

    @ApiOperation("用户添加回执")
    @PostMapping("/feedback/add")
    @ResponseBody
    public JSONResult addFeedback(@RequestParam("order_id") String orderId,
                                  @RequestParam("value") String value,
                                  @RequestParam("rate") int rate,
                                  HttpSession session) {
        String userId = (String) WebUtil.getCurrentUser(session);
        String id = UUID.randomUUID().toString().replace("-","");
        return feedbackService.invertFeedback(id,userId,value,rate,orderId);
    }
}
