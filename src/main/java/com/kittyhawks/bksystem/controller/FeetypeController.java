package com.kittyhawks.bksystem.controller;

import com.kittyhawks.bksystem.entity.Fee_type;
import com.kittyhawks.bksystem.service.FeetypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/feetype")
public class FeetypeController {
    @Autowired
    FeetypeService feetypeService;

    @RequestMapping("/ftupdateById")
    public String updateType(Fee_type record) {
        try {
            return feetypeService.updateById(record) + "";
        } catch (Exception e) {
            e.printStackTrace();
            return "FALSE";
        }
    }


    @RequestMapping("/ftinsert")
    public String insert(Fee_type record) {
        try {
            record.setId(UUID.randomUUID().toString());
            return feetypeService.insert(record) + "";
        } catch (Exception e) {
            e.printStackTrace();
            return "FALSE";
        }
    }
//    @RequestMapping("/ftselectById")
//    public Fee_type selectById(String id) {
//            return feetypeService.selectById(id);
//
//    }

    @RequestMapping("/ftdelete")
    public String delete(String id) {
        try {
            int result = feetypeService.deleteById(id);
            if (result == 0) {
                return "FALSE";
            } else {
                return "TRUE";
            }
        }catch(Exception e){
            return "FALSE";
        }
    }

}