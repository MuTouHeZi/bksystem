package com.kittyhawks.bksystem.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * 页面分发控制类
 * @Author: liangbin
 * @Date: 2018/3/23 18:57
 */
@Controller
@RequestMapping("/bksystem")
public class MainController {
    @RequestMapping("/backsysindex")
    public String backSys(){
        return "backManagerIndex";
    }

    @RequestMapping("welcome")
    public String welcome() {
        return "welcome";
    }

    @RequestMapping("/backsysmarket")
    public String backSysMarket(){
        return "backManagerMarket";
    }

    @RequestMapping("/backsysmarketc")
    public String backSysMarketC(){
        return "backManagerMarketChart";
    }

    @RequestMapping("/backsysfinance")
    public String bakcSysFinance() {
        return "backManagerFinance";
    }

    @RequestMapping("/backsyslogin")
    public String backSysLogin() {
        return "backManagerLogin";
    }

    @RequestMapping(value = "/logout")
    public String logout(HttpSession httpSession){
        httpSession.invalidate();
        return "backManagerLogin";
    }

    @RequestMapping("/orderoperation")
    public String orderOperation() {
        return "backManagerOrderOperation";
    }

    @RequestMapping("/logistics/clothes")
    public String clothes(){
        return "clothes";
    }
    @RequestMapping("/logistics/service")
    public String service(){
        return "service";
    }

    @RequestMapping("/pgrInfo")
    public String pgrInformation(){
        return "pgrInfoSearch";
    }

    @RequestMapping("/financeout")
    public String financeOut() {
        return "backManagerFinanceOut";
    }

    @RequestMapping("/financein")
        public String financeIn() {
        return "backManagerFinanceIn";
    }
}
