package com.kittyhawks.bksystem.controller;

import com.kittyhawks.bksystem.entity.Order;
import com.kittyhawks.bksystem.entity.OrderDTO;
import com.kittyhawks.bksystem.entity.Photographer;
import com.kittyhawks.bksystem.entity.User;
import com.kittyhawks.bksystem.service.OrderService;
import com.kittyhawks.bksystem.service.UserOrderService;
import com.kittyhawks.bksystem.util.JSONResult;
import com.kittyhawks.bksystem.util.WebUtil;
import com.kittyhawks.bksystem.weixin.controller.Base;
import com.kittyhawks.bksystem.weixin.util.WeiXinFinalValue;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.*;

@RequestMapping("/order")
@Api(value="UserOrderController",tags={"订单操作接口新"})
@RestController
public class UserOrderController {

    @Autowired
    private UserOrderService userOrderService;

    @Autowired
    private OrderService orderService;


    @PostMapping("/availableBookTime")
    @ApiOperation("获取指定日期内的可预约时间")
    public JSONResult getAvailableBookTime(String bookDay){
        JSONResult jsonRsult = new JSONResult();
        try{
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(bookDay);
            //List<Integer> list = userOrderService.getAvailableBookTime(date);
            List<Integer> list = new ArrayList<>();
            list.add(1);
            list.add(2);
            list.add(3);
            list.add(4);
            List<String>resultList = new ArrayList<>();
            for(Integer i:list){
                resultList.add(getStringById(i));
            }



            jsonRsult.setStatus(0);
            jsonRsult.setMessage("SUCCESS");
            jsonRsult.setData(resultList);
        }catch(Exception e){
            e.printStackTrace();
            jsonRsult.setStatus(-1);
            jsonRsult.setMessage("FAILURE");
            jsonRsult.setData(e.getMessage());
        }
        return jsonRsult;
    }

    @ApiOperation("获取用户订单列表")
    @GetMapping("/getUserOrder")
    public JSONResult getuserOrderLiset(HttpSession session,String status,String bookDay){
        JSONResult jsonRsult = new JSONResult();
        List<String> statusList = null;
        Date _bookDay = null;
        try{
            if(Base.notEmpty(status)){
                statusList = Arrays.asList(status.split(","));
            }
            if(Base.notEmpty(bookDay)){
                _bookDay = new SimpleDateFormat("yyyy-MM-dd").parse(bookDay);
            }
            User user = (User)WebUtil.getCurrentUser(session);
            if(user==null||user.getId()==null){
                throw new Exception("用户登录状态异常");
            }
            List<Order> list = userOrderService.getUserOrderListById(user.getId(),statusList,_bookDay);
            jsonRsult.setStatus(0);
            jsonRsult.setMessage("SUCCESS");
            jsonRsult.setData(list);
        }catch(Exception e){
            e.printStackTrace();
            jsonRsult.setStatus(-1);
            jsonRsult.setMessage("FAILURE");
            jsonRsult.setData(e.getMessage());
        }
        return jsonRsult;
    }

    @ApiOperation("得到用户订单详情")
    @GetMapping("/userOrderDetail")
    public JSONResult getUserOrderDetail(String id){
        JSONResult jsonRsult = new JSONResult();
        try{
            Order order = userOrderService.getUserOrderDetail(id);
            jsonRsult.setStatus(0);
            jsonRsult.setMessage("SUCCESS");
            jsonRsult.setData(order);
        }catch(Exception e){
            e.printStackTrace();
            jsonRsult.setStatus(-1);
            jsonRsult.setMessage("FAILURE");
            jsonRsult.setData(e.getMessage());
        }
        return jsonRsult;
    }

    @ApiOperation("查询摄影师名下订单")
    @GetMapping("/photographerOrder")
    public JSONResult getPhotographerOrderList(HttpSession session,String status,String bookDay){
        JSONResult jsonRsult = new JSONResult();
        List<String> statusList = null;
        Date _bookDay = null;
        try{

            if(Base.notEmpty(status)){
                 statusList = Arrays.asList(status.split(","));
            }
            if(Base.notEmpty(bookDay)){
                _bookDay = new SimpleDateFormat("yyyy-MM-dd").parse(bookDay);
            }
            Photographer p = (Photographer)WebUtil.getCurrentUser(session);
            if(p==null||p.getId()==null){
                throw new Exception("用户登录状态异常");
            }
            List<Order> list = userOrderService.getPhotographerOrderListById(p.getId(),statusList,_bookDay);
            jsonRsult.setStatus(0);
            jsonRsult.setMessage("SUCCESS");
            jsonRsult.setData(list);
        }catch(Exception e){
            e.printStackTrace();
            jsonRsult.setStatus(-1);
            jsonRsult.setMessage("FAILURE");
            jsonRsult.setData(e.getMessage());
        }
        return jsonRsult;
    }
    @ApiOperation("获取摄影师订单详情")
    @GetMapping("/getPhotographerOrderDetail")
    public JSONResult getPhotographerOrderDetail (String id){
        JSONResult jsonRsult = new JSONResult();
        try{
            Order order = userOrderService.getPhotographerOrderDetail(id);
            jsonRsult.setStatus(0);
            jsonRsult.setMessage("SUCCESS");
            jsonRsult.setData(order);
        }catch(Exception e){
            e.printStackTrace();
            jsonRsult.setStatus(-1);
            jsonRsult.setMessage("FAILURE");
            jsonRsult.setData(e.getMessage());
        }
        return jsonRsult;
    }

    @ApiOperation("后台订单列表条件查询")
    @PostMapping("/orderListInBackground")
    public JSONResult getOrderListInBackground(
                                            String userName,
                                            String photographerName,
                                            String statusList,//格式： "1,2,3,4"
                                            String bookDay,//格式：yyyy-MM-dd
                                            String bookTime,
                                            String mobile
    ){
        JSONResult jsonRsult = new JSONResult();
        try{

            List<String> list = new ArrayList<>(Arrays.asList(statusList.split(",")));
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(bookDay);
            List<OrderDTO> orderList = userOrderService.getOrderListInBackground(userName,photographerName,list,date,
                    bookTime,mobile);
            jsonRsult.setStatus(0);
            jsonRsult.setMessage("SUCCESS");
            jsonRsult.setData(orderList);
        }catch(Exception e){
            e.printStackTrace();
            jsonRsult.setStatus(-1);
            jsonRsult.setMessage("FAILURE");
            jsonRsult.setData(e.getMessage());
        }
        return jsonRsult;
    }
    @ApiOperation(value = "用户添加订单")
    @PostMapping("/user/add")
    public JSONResult userAddOrder(
            String bookDay,//预约天
            String bookTime,//预约场次
            String mobile,//手机号码
            String name,//预约姓名
            String note,//备注
            String department,//学院
            Integer man,//男生人数
            Integer woman,//女生人数
            HttpSession session){
        JSONResult jsonResult = new JSONResult();
        try{
            if(isBlank(bookDay)&&isBlank(bookTime)&&isBlank(mobile)&&isBlank(name)&&man<=0&&woman<=0){
                jsonResult.setStatus(-1);
                jsonResult.setMessage("插入订单失败");
                return jsonResult;
            }
            Date _bookDay = new SimpleDateFormat("yyyy-MM-dd").parse(bookDay);
            Integer _bookTime = getIdByString(bookTime);
            User user = (User) WebUtil.getCurrentUser(session);
            user.setFemaleNum(woman);
            user.setMaleNum(man);
            user.setMobile(mobile);
            user.setUserName(name);
            user.setDepartment(department);
            //String orderId = UUID.randomUUID().toString().replace("-","");
            String orderId = new SimpleDateFormat("yyMMddhhmmss").format(System.currentTimeMillis())+(int)Math.random()*100000;
            Order order = new Order(orderId,_bookDay,_bookTime,user.getId(),man,woman,"0",note);
            boolean result  = orderService.addUserOrder(user,order);
            if(result){
                jsonResult.setStatus(0);
                jsonResult.setMessage("添加订单成功");
                return jsonResult;
            }else{
                jsonResult.setStatus(-1);
                jsonResult.setMessage("插入订单失败");
            }
        }catch(Exception e){
            e.printStackTrace();
            jsonResult.setStatus(-2);
            jsonResult.setMessage("插入操作异常");
        }
        return jsonResult;
    }
    @ApiOperation(value = "得到用户信息")
    @PostMapping("/user/getUserInfo")
    public JSONResult getUserInfo (HttpSession session){
            JSONResult jsonRsult = new JSONResult();
        try{

            jsonRsult.setStatus(0);
            jsonRsult.setMessage("SUCCESS");
            jsonRsult.setData((User)session.getAttribute(WeiXinFinalValue.WX_SESSION_USER));
        }catch(Exception e){
            e.printStackTrace();
            jsonRsult.setStatus(-1);
            jsonRsult.setMessage("FAILURE");
            jsonRsult.setData(e.getMessage());
        }
        return jsonRsult;
    }
    @ApiOperation(value = "撤销订单")
    @GetMapping("/user/cancelOrder")
    public JSONResult cancelOrder(String id){
        JSONResult jsonRsult = new JSONResult();
        try{
            int i = userOrderService.cancelOrder(id);
            if(i>0) {
                jsonRsult.setStatus(0);
                jsonRsult.setMessage("SUCCESS");
                jsonRsult.setData(i);
            }else{
                jsonRsult.setStatus(-1);
                jsonRsult.setMessage("FAILURE");
                jsonRsult.setData("取消订单失败");
            }
        }catch(Exception e){
            e.printStackTrace();
            jsonRsult.setStatus(-1);
            jsonRsult.setMessage("FAILURE");
            jsonRsult.setData(e.getMessage());
        }
        return jsonRsult;
    }

    //根据值转换为id
    public Integer getIdByString(String bookTime){
        switch(bookTime){
            case("第一场(08点-10点)"):
                return 1;
            case("第二场(10点-12点)"):
                return 2;
            case("第三场(13点-15点)"):
                return 3;
            case("第四场(15点-17点)"):
                return 4;
            default:return 0;

        }
    }
    public static boolean isBlank(String str) {
        return str == null || str.trim().isEmpty();
    }
    public static boolean isNotBlank(String str){
        return !isBlank(str);
    }
    public String getStringById(Integer bookTime){
        switch(bookTime){
            case(1):
                return "第一场(08点-10点)";
            case(2):
                return "第二场(10点-12点)";
            case(3):
                return "第三场(13点-15点)";
            case(4):
                return "第四场(15点-17点)";
            default:return null;

        }
    }



/*
    public JSONResult (){
        JSONResult jsonRsult = new JSONResult();
        try{
            userOrderService
            jsonRsult.setStatus(0);
            jsonRsult.setMessage("SUCCESS");
            jsonRsult.setData();
        }catch(Exception e){
            e.printStackTrace();
            jsonRsult.setStatus(-1);
            jsonRsult.setMessage("FAILURE");
            jsonRsult.setData(e.getMessage());
        }
        return jsonRsult;
    }

    public JSONResult (){
        JSONResult jsonRsult = new JSONResult();
        try{
            userOrderService
            jsonRsult.setStatus(0);
            jsonRsult.setMessage("SUCCESS");
            jsonRsult.setData();
        }catch(Exception e){
            e.printStackTrace();
            jsonRsult.setStatus(-1);
            jsonRsult.setMessage("FAILURE");
            jsonRsult.setData(e.getMessage());
        }
        return jsonRsult;
    }

    public JSONResult (){
        JSONResult jsonRsult = new JSONResult();
        try{
            userOrderService
            jsonRsult.setStatus(0);
            jsonRsult.setMessage("SUCCESS");
            jsonRsult.setData();
        }catch(Exception e){
            e.printStackTrace();
            jsonRsult.setStatus(-1);
            jsonRsult.setMessage("FAILURE");
            jsonRsult.setData(e.getMessage());
        }
        return jsonRsult;
    }*/

}
