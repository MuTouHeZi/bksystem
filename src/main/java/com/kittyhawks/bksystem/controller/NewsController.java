package com.kittyhawks.bksystem.controller;

import com.kittyhawks.bksystem.entity.News;
import com.kittyhawks.bksystem.entity.NewsDetailDTO;
import com.kittyhawks.bksystem.service.NewsService;
import com.kittyhawks.bksystem.util.JSONResult;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/index")
public class NewsController {
    @Autowired
    private NewsService newsService;


    @ApiOperation(value = "获取首页新闻")
    @PostMapping("/")
    public List<News> showIndex(){
        try {
            return newsService.getAllNews();
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    @ApiOperation(value = "获取新闻详情")
    @PostMapping("/getNewsDetail")
    public NewsDetailDTO getNewsDetailById(@RequestParam("id") Integer id){
        try {
            return newsService.getNewsDetailById(id);
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
