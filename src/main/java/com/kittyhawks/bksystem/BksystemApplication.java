package com.kittyhawks.bksystem;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@MapperScan("com.kittyhawks.bksystem.dao")
@EnableSwagger2
@EnableScheduling
public class BksystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(BksystemApplication.class, args);
	}

}
