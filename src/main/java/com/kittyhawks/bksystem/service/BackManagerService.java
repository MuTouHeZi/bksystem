package com.kittyhawks.bksystem.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kittyhawks.bksystem.entity.Admin;
import com.kittyhawks.bksystem.entity.BackManagerOrder;
import com.kittyhawks.bksystem.entity.User;
import com.kittyhawks.bksystem.util.JSONResult;

/**
 * 后台管理接口
 * @Author: liangbin
 * @Date: 2018/3/23 14:07
 */
public interface BackManagerService {
    Admin login(String username, String password);
    String getDetailById(String orderid);
    String updateOrder(String orderid,
                       String bookday,
                       String booktime,
                       String price,
                       String status,
                       String message,
                       String total);
}
