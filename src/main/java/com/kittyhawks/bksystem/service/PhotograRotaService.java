package com.kittyhawks.bksystem.service;

import com.kittyhawks.bksystem.entity.PhotograRota;
import com.kittyhawks.bksystem.util.JSONResult;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface PhotograRotaService {
    public List<PhotograRota> findAll();

    public Set findUsableTime();

    @Transactional
    public JSONResult findAvaiablePhotoer(Date bookDay, int bookTime);

    @Transactional
    public int pulledOff(int id);

}
