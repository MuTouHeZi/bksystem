package com.kittyhawks.bksystem.service;

import com.kittyhawks.bksystem.entity.Rota;

import java.util.List;

/**
 *@author 张瀚
 * 后台管理摄影师预约时间Service
 */
public interface PgrRotaService {
    /**
     * 增加摄影师可预约时间
     * @return 成功0，失败1
     */
    String addBook(Rota rota);

    /**
     * 删除摄影师可预约时间
     * @return 成功0，失败1
     */
    String removeBook(Rota rota);

    /**
     * 得到摄影师可预约时间ById
     * @param pgrId
     * @return
     */
    List<Rota> getRota(String pgrId,String currentTime);
}
