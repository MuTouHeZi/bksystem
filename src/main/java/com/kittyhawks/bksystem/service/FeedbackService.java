package com.kittyhawks.bksystem.service;


import com.kittyhawks.bksystem.util.JSONResult;
import org.apache.ibatis.annotations.Param;

public interface FeedbackService {
    public JSONResult invertFeedback(String id,
                                     String userId,
                                     String value,
                                     int rate,
                                     String orderId);
}
