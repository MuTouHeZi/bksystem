package com.kittyhawks.bksystem.service;

import com.kittyhawks.bksystem.entity.User;
import org.apache.ibatis.annotations.Param;

public interface UserService {
    int updateUser(User user);

    int insertByWeChat(String id,String nickname,int sex);
    int insertSelective(User record);
    User selectByPrimaryKeyPro(String id);
}
