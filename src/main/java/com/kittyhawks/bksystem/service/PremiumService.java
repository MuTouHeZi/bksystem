package com.kittyhawks.bksystem.service;

import com.kittyhawks.bksystem.entity.Premium;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;


public interface PremiumService {
    @Transactional
    int insertByUser(List<Premium> premiums);
}
