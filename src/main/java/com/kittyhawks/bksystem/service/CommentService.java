package com.kittyhawks.bksystem.service;

import com.kittyhawks.bksystem.entity.Comment;
import com.kittyhawks.bksystem.util.JSONResult;

public interface CommentService {
    public JSONResult addComment(String id,String user_id,String news_id,String value);

    public JSONResult likeComment(String id);
}
