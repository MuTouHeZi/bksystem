package com.kittyhawks.bksystem.service;

import com.kittyhawks.bksystem.entity.Fee_type;


public interface FeetypeService {

    int insert(Fee_type record);

    int updateById(Fee_type record);

    Fee_type selectById(String id);

    int deleteById(String id);

    String getAllFees();

}
