package com.kittyhawks.bksystem.service;

import com.kittyhawks.bksystem.entity.Order;
import com.kittyhawks.bksystem.entity.User;
import com.kittyhawks.bksystem.util.JSONResult;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import java.util.Map;


public interface OrderService {

     List<Integer> getAvailableBookTime(java.util.Date bookDay);

     Order findByPhotoer(String orderId);

     Order findByUser(String orderId);


     List<Order> findFinishedOrderByUser(String userId);

     List<Order> findUnfinishedOrderByPhotoer(String photoerId);

     List<Order> findFinishedOrderByPhotoer(String photoerId);

     List<Order> findUnfinishedOrderByPhotoerToday(String photoerId);

     List<Order> findFinishedOrderByPhotoerToday(String photoerId);

     @Transactional
     JSONResult addOrderByUser(String orderId,
                               java.sql.Date bookDay,
                               int bookTime,
                               String userId,
                               String photographerId,
                               int totalCount,
                               BigDecimal price,
                               String extraMessage);

     boolean addUserOrder(User user, Order order);

     JSONResult finishOrderByPhr(String orderId);

     JSONResult cancelOrderByUser(String orderId);
}
