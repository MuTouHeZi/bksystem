package com.kittyhawks.bksystem.service.Impl;

import com.kittyhawks.bksystem.dao.PhotograRotaMapper;
import com.kittyhawks.bksystem.entity.PhotograRota;
import com.kittyhawks.bksystem.service.PhotograRotaService;
import com.kittyhawks.bksystem.util.Constant;
import com.kittyhawks.bksystem.util.DateUtil;
import com.kittyhawks.bksystem.util.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class PhotograRotaServiceImpl implements PhotograRotaService{



    @Autowired
    private PhotograRotaMapper photograRotaMapper;

    @Override
    public List<PhotograRota> findAll() {
        return photograRotaMapper.findAll();
    }

    @Override
    public Set findUsableTime() {
        Set bookTimeSet= new HashSet();
        List<PhotograRota> photograRotas = findAll();
        for (PhotograRota photograRota: photograRotas) {
            Date book_day = photograRota.getBookDay();
            int book_time = photograRota.getBookTime();
            String bookday = DateUtil.Date2string(book_day);
            String booktime = bookday + "," + book_time;
            bookTimeSet.add(booktime);
        }
        return bookTimeSet;
    }

    @Override
    public JSONResult findAvaiablePhotoer(Date bookDay, int bookTime) {
        JSONResult jsonResult = new JSONResult();
        List<PhotograRota> photoers = photograRotaMapper.findAvaiablePhotoer(bookDay, bookTime);
        if (photoers.isEmpty()) {
            jsonResult.setStatus(Constant.FAILURE_CODE);
            jsonResult.setMessage("这个时间段没有空闲摄影师");
            jsonResult.setData(null);
        }
        else {
            int length = photoers.size();
            Random random = new Random();
            int i = random.nextInt(length);//随机分配空闲摄影师
            PhotograRota photoer = photoers.get(i);
            String photoerId = photoer.getPgrid();
            int id = photoer.getId();
            jsonResult.setStatus(Constant.SUCCESS_CODE);
            jsonResult.setMessage(id+"");
            jsonResult.setData(photoerId);
            System.out.println(id);
        }
        return jsonResult;
    }



    @Override
    public int pulledOff(int id) {
        return photograRotaMapper.PulledOff(id);
    }

}
