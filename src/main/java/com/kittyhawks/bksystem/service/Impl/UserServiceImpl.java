package com.kittyhawks.bksystem.service.Impl;

import com.kittyhawks.bksystem.dao.UserMapper;
import com.kittyhawks.bksystem.entity.User;
import com.kittyhawks.bksystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserMapper userMapper;

    @Override
    public int updateUser(User user) {
        return userMapper.updateUser(user);
    }

    @Override
    public int insertByWeChat(String id,String nickname,int sex){
        return userMapper.insertByWeChat(id,nickname,sex);
    }

    @Override
    public int insertSelective(User record){
        return userMapper.insertSelective(record);
    }

    public User selectByPrimaryKeyPro(String id){
        return userMapper.selectByPrimaryKeyPro(id);
    }
}
