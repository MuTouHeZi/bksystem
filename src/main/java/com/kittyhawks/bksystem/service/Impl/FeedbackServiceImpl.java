package com.kittyhawks.bksystem.service.Impl;

import com.kittyhawks.bksystem.dao.FeedbackMapper;
import com.kittyhawks.bksystem.service.FeedbackService;
import com.kittyhawks.bksystem.util.Constant;
import com.kittyhawks.bksystem.util.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FeedbackServiceImpl implements FeedbackService{
    @Autowired
    private FeedbackMapper feedbackMapper;

    @Override
    public JSONResult invertFeedback(String id, String userId, String value, int rate,String orderId) {
        JSONResult jsonResult = new JSONResult();
        try{
            feedbackMapper.insertFeedback(id,userId,value,rate,orderId);
            jsonResult.setStatus(Constant.SUCCESS_CODE);
            jsonResult.setMessage("success");
        }catch (Exception e){
            jsonResult.setStatus(Constant.FAILURE_CODE);
            jsonResult.setMessage("failed");
        }
        return jsonResult;
    }
}
