package com.kittyhawks.bksystem.service.Impl;

import com.kittyhawks.bksystem.dao.ExtraServiceMapper;
import com.kittyhawks.bksystem.entity.ExtraService;
import com.kittyhawks.bksystem.entity.Premium;
import com.kittyhawks.bksystem.service.ExtraserviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ExtraserviceServiceImpl implements ExtraserviceService {
    @Autowired
    ExtraServiceMapper extraServicesMapper;
    @Override
    public void insertClothes(ExtraService extraServices) {
         extraServicesMapper.insertClothes(extraServices);
    }

    @Override
    public List<ExtraService> findClothes() {
        return extraServicesMapper.findClothes();
    }

    @Override
    public List<ExtraService> findAvailableService() {
        return extraServicesMapper.findAvaliableService();
    }

    @Override
    public List<ExtraService> findClothesByCondition(String size,String name) {
        return extraServicesMapper.findClothesByCondition(size,name);
    }

    @Override
    public int CleaningClothes(ExtraService extraServices) {
        return extraServicesMapper.CleaningClothes(extraServices);
    }

    @Override
    public int finishCleaning(ExtraService extraServices) {
        return extraServicesMapper.finishCleaning(extraServices);
    }

    @Override
    public int ClothesInfo(ExtraService extraServices) {
        return extraServicesMapper.ClothesInfo(extraServices);
    }

    @Override
    public int insertService(ExtraService extraServices) {
        return extraServicesMapper.insertService(extraServices);
    }

    @Override
    public List<ExtraService> findService() {
        return extraServicesMapper.findService();
    }

    @Override
    public List<ExtraService> findServiceByCondition(String name) {
        return extraServicesMapper.findServiceByCondition(name);
    }

    @Override
    public int setServiceUseable(Integer id) {
        return extraServicesMapper.setServiceUseable(id);
    }

    @Override
    public int setServiceUnUseable(Integer id) {
        return extraServicesMapper.setServiceUnUseable(id);
    }
    //更新服务
     public   int setServiceInfo(ExtraService extraService){
        return extraServicesMapper.setServiceInfo(extraService);
    }
    //订单消耗量
    @Override
    public void useClothes(List<Premium> premiums) {
        int id = 0;
        int amount= 0;
        for (Premium premium: premiums) {
             id = premium.getExtra_id();
             amount = premium.getAmount();
            try {
                extraServicesMapper.useClothes(id,amount);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
