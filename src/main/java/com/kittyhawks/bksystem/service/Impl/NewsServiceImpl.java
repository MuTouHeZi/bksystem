package com.kittyhawks.bksystem.service.Impl;

import com.kittyhawks.bksystem.dao.NewsMapper;
import com.kittyhawks.bksystem.entity.News;
import com.kittyhawks.bksystem.entity.NewsDetail;
import com.kittyhawks.bksystem.entity.NewsDetailDTO;
import com.kittyhawks.bksystem.service.NewsService;
import com.kittyhawks.bksystem.util.Constant;
import com.kittyhawks.bksystem.util.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NewsServiceImpl implements NewsService{

    @Autowired
    private NewsMapper newsMapper;

    @Override
    public List<News> getAllNews(){
        return newsMapper.getAllNews();
    }

    @Override
    public NewsDetailDTO getNewsDetailById(Integer id) {
        NewsDetailDTO dto =  newsMapper.getNewsDetailDTOById(id);
        dto.setList(new ArrayList<NewsDetail>());
        String contents = dto.getContent();
        if(contents==null) return dto;
        String[] contentArray = contents.split(",");
        for(String content:contentArray){
            String[] strs = content.split(":");
            if(strs[0].equals("TEXT")){
                dto.getList().add(newsMapper.getNewsDetailById(Integer.parseInt(strs[1])));
            }else if(strs[0].equals("IMAGE")){
                dto.getList().add(newsMapper.getImageById(Integer.parseInt(strs[1])));
            }
        }
        return dto;
    }

}
