package com.kittyhawks.bksystem.service.Impl;

import com.kittyhawks.bksystem.dao.OrderMapper;
import com.kittyhawks.bksystem.entity.BookingInfo;
import com.kittyhawks.bksystem.entity.Order;
import com.kittyhawks.bksystem.service.MarketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by dell on 2018/3/24.
 */
@Service
public class MarketServiceImpl implements MarketService {

    @Autowired
    OrderMapper orderMapper;

    @Override
    public List<BookingInfo> getOrderList() {
        return orderMapper.getOrderInfoList();
    }

    @Override
    public Order getOrderDetail(String id) {
        Order order = orderMapper.getOrderDetailInfo(id);

        return order;

    }

    @Override
    public List<Map> getCollegeRatio() {
        List<Map> colRatio = orderMapper.getCollegeRatio();

        return colRatio;
    }

    @Override
    public List<Map> getOrderNumOfMonth() {
        List<Map> orderNum = orderMapper.getOrderNumOfMonth();


        return orderNum;
    }

    @Override
    public List<Map> getOrderNumOfDay() {
        List<Map> orderNum = orderMapper.getOrderNumOfDay();
        return orderNum;
    }

    @Override
    public List<Map> getOrderNumOfBTime() {
        List<Map> orderNUm = orderMapper.getOrderNumOfBTime();
        return orderNUm;
    }

    @Override
    public List<Map> getOrderNumOfDayInMonth() {
        List<Map> orderNum = orderMapper.getOrderNumOfDayInMonth();
        return orderNum;
    }

//    @Override
//    public double getMarketShare(int stuCount) {
//        int orderTotalCount = orderMapper.getOrderTotalPCount();
//        return orderTotalCount*1.0/stuCount;
//    }
}
