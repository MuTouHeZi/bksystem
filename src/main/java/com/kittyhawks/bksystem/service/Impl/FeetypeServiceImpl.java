package com.kittyhawks.bksystem.service.Impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kittyhawks.bksystem.dao.FeeMapper;
import com.kittyhawks.bksystem.dao.Fee_typeMapper;

import com.kittyhawks.bksystem.entity.Fee_type;
import com.kittyhawks.bksystem.service.FeetypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FeetypeServiceImpl implements FeetypeService {
    @Autowired
    Fee_typeMapper fee_typeMapper;

    @Autowired
    FeeMapper feeMapper;

    @Autowired
    ObjectMapper objectMapper;

    @Override
    public int deleteById(String id) {
        return fee_typeMapper.deleteById(id);
    }

    @Override
    public int insert(Fee_type record) {
        return fee_typeMapper.insert(record);
    }

    @Override
    public int updateById(Fee_type record) {
        return fee_typeMapper.updateById(record);
    }

    @Override
    public Fee_type selectById(String id){return fee_typeMapper.selectById(id);}

    @Override
    public String getAllFees() {
        String fees = "";
        try {
            fees = objectMapper.writeValueAsString(feeMapper.getAllFees());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return fees;
    }

}
