package com.kittyhawks.bksystem.service.Impl;

import com.kittyhawks.bksystem.dao.RotaMapper;
import com.kittyhawks.bksystem.entity.Rota;
import com.kittyhawks.bksystem.service.PgrRotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PgrRotaServiceImpl implements PgrRotaService {

    @Autowired
    RotaMapper mapper;

    /**
     * 增加摄影师可预约时间
     *
     * @return 成功0，失败1
     */
    @Override
    public String addBook(Rota rota) {

        try {
            mapper.addBook(rota);
            return "0";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "1";
    }

    /**
     * 删除摄影师可预约时间
     *
     * @return 成功0，失败1
     */
    @Override
    public String removeBook(Rota rota) {

        try {
            mapper.removeBook(rota);
            return "0";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "1";
    }

    /**
     * 得到摄影师可预约时间ById
     *
     * @param pgrId
     * @return Rota
     */
    @Override
    public List<Rota> getRota(String pgrId,String currentDate) {

        return mapper.selectRotaById(pgrId,currentDate);
    }
}
