package com.kittyhawks.bksystem.service.Impl;

import com.kittyhawks.bksystem.dao.CommentMapper;
import com.kittyhawks.bksystem.entity.Comment;
import com.kittyhawks.bksystem.service.CommentService;
import com.kittyhawks.bksystem.util.Constant;
import com.kittyhawks.bksystem.util.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImpl implements CommentService{
    @Autowired
    private CommentMapper commentMapper;


    @Override
    public JSONResult addComment(String id, String user_id, String news_id, String value) {
        JSONResult jsonResult = new JSONResult();
        try{
            commentMapper.insertComment(id, user_id,news_id,value);
            jsonResult.setStatus(Constant.SUCCESS_CODE);
            jsonResult.setMessage("success");
        }catch (Exception e){
            jsonResult.setStatus(Constant.FAILURE_CODE);
            jsonResult.setMessage("failed");
        }
        return jsonResult;
    }

    @Override
    public JSONResult likeComment(String id) {
        JSONResult jsonResult = new JSONResult();
        try{
            commentMapper.likeComment(id);
            jsonResult.setStatus(Constant.SUCCESS_CODE);
            jsonResult.setMessage("success");
        }catch (Exception e){
            jsonResult.setStatus(Constant.FAILURE_CODE);
            jsonResult.setMessage("failed");
        }
        return jsonResult;
    }
}
