package com.kittyhawks.bksystem.service.Impl;

import com.kittyhawks.bksystem.dao.OrderMapper;
import com.kittyhawks.bksystem.dao.PhotograRotaMapper;
import com.kittyhawks.bksystem.dao.UserMapper;
import com.kittyhawks.bksystem.dao.UserOrderMapper;
import com.kittyhawks.bksystem.entity.Order;
import com.kittyhawks.bksystem.entity.Photographer;
import com.kittyhawks.bksystem.entity.User;
import com.kittyhawks.bksystem.service.OrderService;
import com.kittyhawks.bksystem.util.Constant;
import com.kittyhawks.bksystem.util.DateUtil;
import com.kittyhawks.bksystem.util.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Service
public class OrderServiceImpl implements OrderService{

    @Autowired
    protected OrderMapper orderMapper;

    @Autowired
    protected UserMapper userMapper;

    @Autowired
    protected UserOrderMapper userOrderMapper;

    @Autowired
    protected PhotograRotaMapper photograRotaMapper;

    @Override
    public List<Integer> getAvailableBookTime(Date bookDay){
        return orderMapper.getAvailableBookTime(bookDay);
    }
    @Override
    public Order findByPhotoer(String orderId) {
        return orderMapper.findByPhotoer(orderId);
    }

    @Override
    public Order findByUser(String orderId) {
        return orderMapper.findByUser(orderId);
    }



    @Override
    public List<Order> findFinishedOrderByUser(String userId) {
        return orderMapper.findFinishedOrderByUser(userId);
    }

    @Override
    public List<Order> findUnfinishedOrderByPhotoer(String photoerId) {
        return orderMapper.findUnfinishedOrderByPhotoer(photoerId);
    }

    @Override
    public List<Order> findFinishedOrderByPhotoer(String photoerId) {
        return orderMapper.findFinishedOrderByPhotoer(photoerId);
    }

    @Override
    public List<Order> findUnfinishedOrderByPhotoerToday(String photoerId) {
        String bookDay = DateUtil.Date2String(new Date());
        return orderMapper.findUnfinishedOrderByPhotoerToday(photoerId,bookDay);
    }

    @Override
    public List<Order> findFinishedOrderByPhotoerToday(String photoerId) {
        String bookDay = DateUtil.Date2String(new Date());
        return orderMapper.findFinishedOrderByPhotoerToday(photoerId,bookDay);
    }

    @Override
    public JSONResult addOrderByUser(String orderId,
                                     java.sql.Date bookDay,
                                     int bookTime,
                                     String userId,
                                     String photographerId,
                                     int totalCount,
                                     BigDecimal price,
                                     String extraMessage) {
        JSONResult jsonResult = new JSONResult();
        Date createTime = new Date();
        try {
            int flag = orderMapper.addOrderByUser(orderId,bookDay, bookTime, userId, photographerId,
                    totalCount, createTime, price, extraMessage);
            jsonResult.setStatus(Constant.SUCCESS_CODE);
        }catch (Exception e){
            jsonResult.setStatus(Constant.FAILURE_CODE);
            jsonResult.setMessage("Order 添加失败"+e);
        }
        return jsonResult;
    }

    @Transactional
    @Override
    public boolean addUserOrder(User user, Order order) {
        Photographer photographer = userOrderMapper.getAvailablePhotographer(order.getBookDay(),order.getBookTime());
        order.setPhotographerId(photographer.getId());
        order.setPrice(new BigDecimal(photographer.getServicePrice()*(order.getMaleNum()+order.getFemaleNum())));
        int up = userMapper.updateUser(user);
        int in = userOrderMapper.addNewOrder(order);
        int poto =photograRotaMapper.updateRota2Used(photographer.getId(),order.getBookDay(),order.getBookTime());
        if(photographer.getId()!=null&&photographer.getId()!=""&&up>0&&in>0&&poto>0){
            return true;
        }
            return false;
    }


    @Override
    public JSONResult finishOrderByPhr(String orderId) {
        JSONResult jsonResult = new JSONResult();
        try{
            orderMapper.finishOrderByPhr(orderId);
            jsonResult.setStatus(Constant.SUCCESS_CODE);
            jsonResult.setMessage("success");
        }catch (Exception e){
            e.printStackTrace();
            jsonResult.setStatus(Constant.FAILURE_CODE);
            jsonResult.setMessage("failed");
        }
        return jsonResult;
    }

    @Override
    public JSONResult cancelOrderByUser(String orderId) {
        JSONResult jsonResult = new JSONResult();
        try{
            orderMapper.CancelOrderByUser(orderId);
            jsonResult.setStatus(Constant.SUCCESS_CODE);
            jsonResult.setMessage("success");
        }catch (Exception e){
            e.printStackTrace();
            jsonResult.setStatus(Constant.FAILURE_CODE);
            jsonResult.setMessage("failed");
        }
        return jsonResult;
    }
}
