package com.kittyhawks.bksystem.service.Impl;

import com.kittyhawks.bksystem.dao.PremiumMapper;
import com.kittyhawks.bksystem.entity.Premium;
import com.kittyhawks.bksystem.service.PremiumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PremiumServiceImpl implements PremiumService{
    @Autowired
    private PremiumMapper premiumMapper;


    @Override
    public int insertByUser(List<Premium> premiums) {
        return premiumMapper.insertByUser(premiums);
    }
}
