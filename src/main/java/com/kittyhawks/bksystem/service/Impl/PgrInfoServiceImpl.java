package com.kittyhawks.bksystem.service.Impl;

import com.kittyhawks.bksystem.dao.PhotographerMapper;
import com.kittyhawks.bksystem.entity.Photographer;
import com.kittyhawks.bksystem.service.PgrInfoService;
import com.kittyhawks.bksystem.util.Constant;
import com.kittyhawks.bksystem.util.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 张瀚
 */
@Service
public class PgrInfoServiceImpl implements PgrInfoService {

    @Autowired
    PhotographerMapper photographerMapper;

    static final String SUCCESSED_CODE = "0";
    static final String FAILED_CODE = "1";


    @Override
    public String insertInfo(Photographer record) {

        try {
            photographerMapper.insert(record);
            return SUCCESSED_CODE;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return FAILED_CODE;
    }

    @Override
    public List<Photographer> getAllInfo(String company) {

        return photographerMapper.selectAllInfo(company);
    }

    @Override
    public List<Photographer> getInfoByNameAndCompany(String name, String company) {
        return photographerMapper.selectInfoByNameAndCompany(name, company);
    }

    @Override
    public Photographer getInfoById(String id) {
        return photographerMapper.selectByPrimaryKey(id);
    }


    /*查询摄影师信息（包括微信信息）*/
    @Override
    public Photographer selectByOpenid(String openid){
        return photographerMapper.selectByOpenid(openid);
    }

//    @Override
//    public List<String> getAllCompany() {
//        return photographerMapper.selectAllCompany();
//    }

    @Override
    public String alterInfo(Photographer record) {
        try {
            photographerMapper.alterInfo(record);
            return SUCCESSED_CODE;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return FAILED_CODE;
    }

    @Override
    public int updateByPrimaryKeySelective(Photographer record){
        return photographerMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public String deleteInfoById(String id) {

        try {
            photographerMapper.deleteInfoById(id);
            return SUCCESSED_CODE;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return FAILED_CODE;
    }

    @Override
    public List<Photographer> selectInfo() {
        List<Photographer> photographer = photographerMapper.selectInfo();
        return photographer;
    }

    @Override
    public List<Photographer> selectInfoByName(String name) {
        List<Photographer> photographer = photographerMapper.selectInfoByName(name);
        return photographer;
    }

    @Override
    public List<Photographer> selectInfoByCompany(String company) {
        List<Photographer> photographer = photographerMapper.selectInfoByCompany(company);
        return photographer;
    }

    @Override
    public List<Photographer> selectInfoByNameAndCompany(String name, String company) {
        List<Photographer> photographer = photographerMapper.selectInfoByNameAndCompany(name, company);
        return photographer;
    }

    @Override
    public JSONResult checkPhr(String username,String password) {
        JSONResult jsonResult = new JSONResult();
        String truePassword = photographerMapper.checkPhr(username);
        if(truePassword.equals(password.trim())){
            jsonResult.setStatus(Constant.SUCCESS_CODE);
            jsonResult.setMessage("success");
        }else{
            jsonResult.setStatus(Constant.FAILURE_CODE);
            jsonResult.setMessage("用户名密码不匹配");
        }
        return jsonResult;
    }

}
