package com.kittyhawks.bksystem.service.Impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kittyhawks.bksystem.dao.FeeMapper;
import com.kittyhawks.bksystem.dao.OrderMapper;
import com.kittyhawks.bksystem.entity.Fee;
import com.kittyhawks.bksystem.entity.FinishOrder;
import com.kittyhawks.bksystem.service.FinanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FinanceServiceImpl implements FinanceService{

    @Autowired
    FeeMapper feeMapper;

    @Autowired
    OrderMapper orderMapper;

    @Autowired
    ObjectMapper objectMapper;

    @Override
    public int deleteById(String id) {
        return feeMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Fee fee) {
        return feeMapper.insert(fee);
    }

    @Override
    public Fee selectById(String id) {
        return feeMapper.selectByPrimaryKey(id);
    }

    public List<Fee> selectFeesByParams(Fee params){
        return feeMapper.selectFeesByParams(params);
    }

    public int updateFee(Fee fee){
        return feeMapper.updateByPrimaryKey(fee);
    }


    public String getFinishOrder() {
        List<FinishOrder> orders = orderMapper.getFinishOrder();
        String result = "";
        try {
            result = objectMapper.writeValueAsString(orders);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }
}
