package com.kittyhawks.bksystem.service.Impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kittyhawks.bksystem.dao.AdminMapper;
import com.kittyhawks.bksystem.dao.OrderMapper;
import com.kittyhawks.bksystem.dao.UserMapper;
import com.kittyhawks.bksystem.entity.Admin;
import com.kittyhawks.bksystem.entity.BackManagerOrder;
import com.kittyhawks.bksystem.entity.User;
import com.kittyhawks.bksystem.service.BackManagerService;
import com.kittyhawks.bksystem.util.JSONResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.kittyhawks.bksystem.util.Constant.FAILURE_CODE;
import static com.kittyhawks.bksystem.util.Constant.SUCCESS_CODE;

/**
 * 后台管理
 * @Author: liangbin
 * @Date: 2018/3/23 14:08
 */
@Service
public class BackManagerServiceImpl implements BackManagerService{

    @Autowired
    AdminMapper adminMapper;
    @Autowired
    JSONResult jsonResult;
    @Autowired
    OrderMapper orderMapper;

    ObjectMapper objectMapper = new ObjectMapper();


    @Override
    public Admin login(String username, String passsword) {
        Admin admin = adminMapper.login(username, passsword);
        return admin;
    }

    @Override
    public String getDetailById(String orderid) {
        String order = null;
        try {
            order = objectMapper.writeValueAsString(orderMapper.backManagerOrderDetail(orderid).get(0));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return order;
    }

    @Override
    public String updateOrder(String orderid, String bookday, String booktime, String price, String status, String message, String total) {
        bookday = bookday.substring(0,10);
        Date bookdate = null;
        Integer cbooktime = Integer.valueOf(booktime);
        BigDecimal cprice = new BigDecimal(price);
        Integer cstatus = Integer.valueOf(status);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Integer count = Integer.valueOf(total);
        try {
            bookdate = sdf.parse(bookday);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        orderMapper.updateOrder(bookdate, cbooktime, cprice, cstatus, message, count, orderid);
        return "SUCCESS";
    }
}
