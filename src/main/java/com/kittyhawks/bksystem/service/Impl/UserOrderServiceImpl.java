package com.kittyhawks.bksystem.service.Impl;

import com.kittyhawks.bksystem.dao.OrderMapper;
import com.kittyhawks.bksystem.dao.UserOrderMapper;
import com.kittyhawks.bksystem.entity.Order;
import com.kittyhawks.bksystem.entity.OrderDTO;
import com.kittyhawks.bksystem.service.UserOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
@Service
public class UserOrderServiceImpl implements UserOrderService {
    @Autowired
    private UserOrderMapper userMapper;

    @Autowired
    private OrderMapper orderMapper;
    @Override
    public List<Order> getUserOrderListById(String id, List<String> statusList, Date bookDay) {
        return userMapper.getUserOrderListById(id,statusList,bookDay);
    }

    @Override
    public Order getUserOrderDetail(String id) {
        return userMapper.getUserOrderDetail(id);
    }

    @Override
    public List<Order> getPhotographerOrderListById(String id, List<String> statusList, Date bookDay) {
        return userMapper.getPhotographerOrderListById(id,statusList,bookDay);
    }

    @Override
    public Order getPhotographerOrderDetail(String id) {
        return userMapper.getPhotographerOrderDetail(id);
    }

    @Override
    public List<OrderDTO> getOrderListInBackground(String userName, String photographerName, List<String> statusList, Date bookDay, String bookTime, String mobile) {
        return userMapper.getOrderListInBackground(userName,photographerName,statusList,bookDay,bookTime,mobile);
    }

    @Override
    public int addNewOrder(Order order) {
        return userMapper.addNewOrder(order);
    }

    @Override
    public int updateOrder(Order order) {
        return userMapper.updateOrder(order);
    }

    @Override
    public List<Integer> getAvailableBookTime(Date bookDay) {
        return userMapper.getAvailableBookTime(bookDay);
    }

    @Override
    public int cancelOrder(String id) {
        return orderMapper.CancelOrderByUser(id);
    }
}
