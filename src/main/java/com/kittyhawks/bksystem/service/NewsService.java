package com.kittyhawks.bksystem.service;

import com.kittyhawks.bksystem.entity.News;
import com.kittyhawks.bksystem.entity.NewsDetailDTO;
import com.kittyhawks.bksystem.util.JSONResult;

import java.util.List;

public interface NewsService {
    List<News> getAllNews();

    NewsDetailDTO getNewsDetailById(Integer id);
}
