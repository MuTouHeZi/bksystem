package com.kittyhawks.bksystem.service;

import com.kittyhawks.bksystem.entity.BookingInfo;
import com.kittyhawks.bksystem.entity.Order;


import java.util.List;
import java.util.Map;

/**
 * Created by dell on 2018/3/24.
 */
public interface MarketService {
    List<BookingInfo> getOrderList();//客户预约信息
    Order getOrderDetail(String id);//订单具体信息
    List<Map> getCollegeRatio();//获取学院预约人数所占比例
    List<Map> getOrderNumOfMonth();//获取一年内每月订单数
    List<Map> getOrderNumOfDay();//获取一周内每天订单数
    List<Map> getOrderNumOfBTime();//统计每个时间段的订单总数
    List<Map> getOrderNumOfDayInMonth();//统计当月每天预约订单数
//    double  getMarketShare(int stuCount);
}
