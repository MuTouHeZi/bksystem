package com.kittyhawks.bksystem.service;

import com.kittyhawks.bksystem.entity.Photographer;
import com.kittyhawks.bksystem.util.JSONResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 张瀚
 * @后台管理摄影师信息Service
 */
public interface PgrInfoService {

    String insertInfo(Photographer record);

    /*查询所有摄影师信息*/
    List<Photographer> getAllInfo(String company);

    /*查询摄影师信息By名字和所属公司*/
    List<Photographer> getInfoByNameAndCompany(String name, String company);

    /*查询摄影师信息ById*/
    Photographer getInfoById(String id);

    /*查询摄影师信息by openid（包括微信信息）*/
    Photographer selectByOpenid(String openid);

    /*修改摄影师信息*/
    String alterInfo(Photographer record);

    /*修改摄影师信息 详细*/
    int updateByPrimaryKeySelective(Photographer record);

    /**
     * 删除摄影师信息ById
     * @param id
     * @return 成功"0",失败"1"
     */
    String deleteInfoById(String id);

    List<Photographer> selectInfo();

    List<Photographer> selectInfoByName(String name);

    List<Photographer> selectInfoByCompany(String company);

    List<Photographer> selectInfoByNameAndCompany(String name,String company);

    JSONResult checkPhr(String username,String password);
}
