package com.kittyhawks.bksystem.service;

import com.kittyhawks.bksystem.entity.Fee;
import com.kittyhawks.bksystem.entity.Order;

import java.util.List;

public interface FinanceService {
//  fee表
    public int deleteById(String id);
    public int insert(Fee fee);
    public Fee selectById(String id);
    public int updateFee(Fee fee);
    List<Fee> selectFeesByParams(Fee feeParm);
    String getFinishOrder();
}
