package com.kittyhawks.bksystem.dao;

import com.kittyhawks.bksystem.entity.BackManagerOrder;
import com.kittyhawks.bksystem.entity.BookingInfo;
import com.kittyhawks.bksystem.entity.FinishOrder;
import com.kittyhawks.bksystem.entity.Order;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public interface OrderMapper {

     List<Integer> getAvailableBookTime(Date bookDay);

     Order findByPhotoer(@Param("id") String orderId);

     Order findByUser(@Param("id") String orderId);

     List<Order> findUnfinishedOrderByUser(@Param("userId")String userId);

     List<Order> findFinishedOrderByUser(String userId);

     List<Order> findUnfinishedOrderByPhotoer(String photoerId);

     List<Order> findFinishedOrderByPhotoer(String photoerId);

     List<Order> findUnfinishedOrderByPhotoerToday(@Param("photoerId") String photoerId,
                                                   @Param("bookday") String bookday);

     List<Order> findFinishedOrderByPhotoerToday(@Param("photoerId") String photoerId,
                                                 @Param("bookday") String bookDay);

     int addOrderByUser(@Param("orderId") String orderId,
                        @Param("bookDay") java.sql.Date bookDay,
                        @Param("bookTime") int bookTime,
                        @Param("userId") String userId,
                        @Param("photographerId")String photographerId,
                        @Param("totalCount")int totalCount,
                        @Param("createTime")Date createTime,
                        @Param("price")BigDecimal price,
                        @Param("extraMessage")String extraMessage);

    int addNewOrder(Order order);


    List<BookingInfo> getOrderInfoList();//订单-用户列表

    Order getOrderDetailInfo(String id);//订单具体信息

    List<Map> getCollegeRatio();//获取学院预约人数所占总预约人数比例

    List<Map> getOrderNumOfMonth();//统计每月预约订单数

    List<Map> getOrderNumOfDay();//统计一周内每天预约订单数

    List<Map> getOrderNumOfBTime();//统计每个时间段的订单总数

    List<Map> getOrderNumOfDayInMonth();//统计当月每天预约订单数

    int getOrderTotalPCount();//计算所有订单总人数

    List<FinishOrder> getFinishOrder();

    void deleteorder(@Param("orderid") String orderid);

    int finishOrderByPhr(@Param("id") String orderId);

    int CancelOrderByUser(@Param("id") String orderId);

    List<BackManagerOrder> backManagerOrderDetail(@Param("id") String orderid);

    void updateOrder(@Param("book_day") Date book_day, @Param("book_time") Integer book_time, @Param("price") BigDecimal price, @Param("status") Integer status, @Param("extra_message") String extra_message, @Param("total_count") Integer total_count, @Param("id") String id);
}