package com.kittyhawks.bksystem.dao;

import com.kittyhawks.bksystem.entity.Rota;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RotaMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Rota record);

    int insertSelective(Rota record);

    Rota selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Rota record);

    int updateByPrimaryKey(Rota record);

    /**
     * 增加摄影师可预约时间
     */
    void addBook(@Param("Rota") Rota rota);

    /**
     * 删除摄影师可预约时间
     */
    void removeBook(@Param("Rota") Rota rota);

    /**
     * 查询摄影师可预约时间ById
     *
     * @param pgrId
     * @return Rota
     */
    List<Rota> selectRotaById(@Param("pgrid") String pgrId,@Param("currentDate") String currentDate);
}