package com.kittyhawks.bksystem.dao;

import com.kittyhawks.bksystem.entity.ExtraService;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
@Repository
public interface ExtraServiceMapper {
    /**
     * 衣服接口
     */
    /**
     * Insert
     */
    //插入衣服
    int insertClothes(ExtraService extraServices);
    /**
     * Select
     */
    //查询所有衣服
    List<ExtraService> findClothes();

    /**查询可用的衣服**/
    List<ExtraService> findAvaliableService();

    //条件查询衣服
    List<ExtraService> findClothesByCondition(@Param("size") String size,@Param("name") String name);
    /**
     * Update
     */
//    //设置衣服损耗
//    int setClothesDamageAmount(ExtraService extraServices);
    //清洗衣服
    int CleaningClothes(ExtraService extraServices);
    //衣服清洗完成
    int finishCleaning(ExtraService extraServices);
    //更新衣服信息
    int ClothesInfo(ExtraService record);

    /**
     * 服务接口
     */
    /**
     * Insert
     */
    //插入服务
    int insertService(ExtraService extraServices);
    /**
     * Select
     */
    //查询所有服务
    List<ExtraService> findService();
    //条件查询服务
     List<ExtraService> findServiceByCondition(String name);
    /**
     * Update
     */
    //设置服务可用
    int setServiceUseable(Integer id);
    //设置服务不可用
    int setServiceUnUseable(Integer id);
    //更新服务
    int setServiceInfo(ExtraService extraService);
    //订单消耗量
    int useClothes(@Param("id") Integer id,@Param("amount") Integer amount);
}