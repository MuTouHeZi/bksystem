package com.kittyhawks.bksystem.dao;

import com.kittyhawks.bksystem.entity.Photo;

public interface PhotoMapper {
    int insert(Photo record);

    int insertSelective(Photo record);
}