package com.kittyhawks.bksystem.dao;

import com.kittyhawks.bksystem.entity.Comment;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentMapper {
    public int insertComment(@Param("id") String id,
                             @Param("user_id") String userId,
                             @Param("news_id") String newsId,
                             @Param("value") String value);

    public int likeComment(@Param("id") String id);
}
