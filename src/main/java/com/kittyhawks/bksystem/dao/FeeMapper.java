package com.kittyhawks.bksystem.dao;

import com.kittyhawks.bksystem.entity.Fee;
import com.kittyhawks.bksystem.entity.Order;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FeeMapper {
    int deleteByPrimaryKey(String id);

    int insert(Fee record);

    Fee selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Fee record);

    int updateByPrimaryKey(Fee record);

    List<Fee> selectFeesByParams(Fee params);

    List<Fee> getAllFees();

}