package com.kittyhawks.bksystem.dao;

import com.kittyhawks.bksystem.entity.PhotograRota;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
@Repository
public interface PhotograRotaMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PhotograRota record);

    int insertSelective(PhotograRota record);

    PhotograRota selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PhotograRota record);

    int updateByPrimaryKey(PhotograRota record);

    List<PhotograRota> findAll();

    public List<PhotograRota> findAvaiablePhotoer(@Param("bookDay") Date bookDay, @Param("bookTime") int bookTime);

    public int PulledOff(int id);

    int updateRota2Used(@Param("pgrid") String pgrid,@Param("bookDay")Date bookDay,@Param("bookTime")int bookTime);
}