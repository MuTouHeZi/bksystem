package com.kittyhawks.bksystem.dao;

import com.kittyhawks.bksystem.entity.Fee_type;
import org.springframework.stereotype.Repository;

@Repository
public interface Fee_typeMapper {
    int insert(Fee_type record);

    int insertSelective(Fee_type record);

    int updateById(Fee_type record);

    Fee_type selectById(String id);

    int deleteById(String id);
}