package com.kittyhawks.bksystem.dao;

import com.kittyhawks.bksystem.entity.Image;
import com.kittyhawks.bksystem.entity.News;
import com.kittyhawks.bksystem.entity.NewsDetail;
import com.kittyhawks.bksystem.entity.NewsDetailDTO;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface NewsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(News record);

    int insertSelective(News record);

    News selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(News record);

    int updateByPrimaryKey(News record);

    List<News> getAllNews();

    NewsDetailDTO getNewsDetailDTOById(Integer id);

    NewsDetail getImageById(Integer id);

    NewsDetail getNewsDetailById(Integer id);

}