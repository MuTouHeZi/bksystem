package com.kittyhawks.bksystem.dao;

import com.kittyhawks.bksystem.entity.Admin;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminMapper {
    int deleteByPrimaryKey(String id);

    int insert(Admin record);

    int insertSelective(Admin record);

    Admin selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Admin record);

    int updateByPrimaryKey(Admin record);

    Admin login(@Param("username") String username, @Param("password") String password);

    /**
     *
     * @param username
     * @return admin管理员的对应remark属性的权限
     */
    Integer getAuthority(@Param("username") String username);

    /**
     *查询管理员主键
     * @param username
     * @return
     */
    String getIdByUsername(@Param("username") String username);
}