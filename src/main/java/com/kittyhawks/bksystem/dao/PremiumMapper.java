package com.kittyhawks.bksystem.dao;

import com.kittyhawks.bksystem.entity.Premium;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface PremiumMapper {
    int insert(Premium record);

    int insertSelective(Premium record);

    int insertByUser(List<Premium> premiums);
}