package com.kittyhawks.bksystem.dao;

import com.kittyhawks.bksystem.entity.Photographer;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhotographerMapper {
    int deleteByPrimaryKey(String id);

    int insert(Photographer record);

    int insertSelective(Photographer record);

    Photographer selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Photographer record);

    int updateByPrimaryKey(Photographer record);

    /*查询所有摄影师信息*/
    List<Photographer> selectAllInfo(@Param("company") String company);

    /*查询摄影师信息By名字和所属公司*/
//    List<Photographer> selectInfoByNameAndCompany(@Param("name") String name, @Param("company") String company);

    /*查询所有公司*/
    List<String> selectAllCompany();

    /*修改摄影师信息*/
    int alterInfo(@Param("Photographer") Photographer photographer);

    /*删除摄影师信息ById*/
    int deleteInfoById(@Param("id") String id);

    /*查询摄影师信息by openid（包括微信信息）*/
    Photographer selectByOpenid(String openid);

    List<Photographer> selectInfo();

    List<Photographer> selectInfoByName(@Param("name") String name);

    List<Photographer> selectInfoByCompany(@Param("company") String company);

    List<Photographer> selectInfoByNameAndCompany(@Param("name") String name,@Param("company") String company);

    String checkPhr(@Param("username") String username);
}