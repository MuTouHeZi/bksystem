package com.kittyhawks.bksystem.dao;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FeedbackMapper {
    public int insertFeedback(@Param("id") String id,
                              @Param("user_id") String userId,
                              @Param("value") String value,
                              @Param("rate") int rate,
                              @Param("order_id") String orderId);
}
