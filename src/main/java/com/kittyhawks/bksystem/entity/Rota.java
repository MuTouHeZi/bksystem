package com.kittyhawks.bksystem.entity;

import java.util.Date;

public class Rota {
    private Integer id;

    private String pgrid;

    private Date bookDay;

    private Integer bookTime;

    private Byte acceptStatus;

    private Byte status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPgrid() {
        return pgrid;
    }

    public void setPgrid(String pgrid) {
        this.pgrid = pgrid == null ? null : pgrid.trim();
    }

    public Date getBookDay() {
        return bookDay;
    }

    public void setBookDay(Date bookDay) {
        this.bookDay = bookDay;
    }

    public Integer getBookTime() {
        return bookTime;
    }

    public void setBookTime(Integer bookTime) {
        this.bookTime = bookTime;
    }

    public Byte getAcceptStatus() {
        return acceptStatus;
    }

    public void setAcceptStatus(Byte acceptStatus) {
        this.acceptStatus = acceptStatus;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Rota other = (Rota) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
                && (this.getPgrid() == null ? other.getPgrid() == null : this.getPgrid().equals(other.getPgrid()))
                && (this.getBookDay() == null ? other.getBookDay() == null : this.getBookDay().equals(other.getBookDay()))
                && (this.getBookTime() == null ? other.getBookTime() == null : this.getBookTime().equals(other.getBookTime()))
                && (this.getAcceptStatus() == null ? other.getAcceptStatus() == null : this.getAcceptStatus().equals(other.getAcceptStatus()))
                && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getPgrid() == null) ? 0 : getPgrid().hashCode());
        result = prime * result + ((getBookDay() == null) ? 0 : getBookDay().hashCode());
        result = prime * result + ((getBookTime() == null) ? 0 : getBookTime().hashCode());
        result = prime * result + ((getAcceptStatus() == null) ? 0 : getAcceptStatus().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        return result;
    }

    public Rota(String pgrid, Date bookDay, Integer bookTime) {
        this.pgrid = pgrid;
        this.bookDay = bookDay;
        this.bookTime = bookTime;
    }

    public Rota() {
    }

    @Override
    public String toString() {
        return "Rota{" +
                "id=" + id +
                ", pgrid='" + pgrid + '\'' +
                ", bookDay=" + bookDay +
                ", bookTime=" + bookTime +
                ", acceptStatus=" + acceptStatus +
                ", status=" + status +
                '}';
    }
}