package com.kittyhawks.bksystem.entity;

import javax.sql.rowset.spi.SyncResolver;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: liangbin
 * @Date: 2018/4/7 22:50
 */
public class FinishOrder {
    private String id;
    private Date bookday;
    private int booktime;
    private BigDecimal price;
    private String name;
    private String depart;
    private String batch;
    private String mobile;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getBookday() {
        return bookday;
    }

    public void setBookday(Date bookday) {
        this.bookday = bookday;
    }

    public int getBooktime() {
        return booktime;
    }

    public void setBooktime(int booktime) {
        this.booktime = booktime;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
