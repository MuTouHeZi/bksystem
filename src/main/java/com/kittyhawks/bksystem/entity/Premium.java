package com.kittyhawks.bksystem.entity;

public class Premium {
    private String id;

    private String orderId;

    private Integer amount;

    private Byte status;

    private Byte extraType;

    private ExtraService service;

    private Integer extra_id;

    public Integer getExtra_id() {
        return extra_id;
    }

    public void setExtra_id(Integer extra_id) {
        this.extra_id = extra_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId == null ? null : orderId.trim();
    }


    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Byte getExtraType() {
        return extraType;
    }

    public void setExtraType(Byte extraType) {
        this.extraType = extraType;
    }

    public ExtraService getService() {
        return service;
    }

    public void setService(ExtraService service) {
        this.service = service;
    }
}