package com.kittyhawks.bksystem.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author: liangbin
 * @Date: 2018/4/11 15:45
 */
public class BackManagerOrder {
    private String id;
    private Date bookday;
    private int booktime;
    private Date endTime;
    private BigDecimal price;
    private Integer status;
    private String message;
    private String pname;
    private String pmobile;
    private String pcompany;
    private String personnum;
    private String uname;
    private String umobile;
    private String udepart;
    private String umajor;
    private String ubatch;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getBookday() {
        return bookday;
    }

    @JsonFormat(pattern = "yyyy-MM-dd")
    public void setBookday(Date bookday) {
        this.bookday = bookday;
    }

    public int getBooktime() {
        return booktime;
    }

    public void setBooktime(int booktime) {
        this.booktime = booktime;
    }

    public Date getEndTime() {
        return endTime;
    }

    @JsonFormat(pattern = "yyyy-MM-dd")
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getPmobile() {
        return pmobile;
    }

    public void setPmobile(String pmobile) {
        this.pmobile = pmobile;
    }

    public String getPcompany() {
        return pcompany;
    }

    public void setPcompany(String pcompany) {
        this.pcompany = pcompany;
    }

    public String getPersonnum() {
        return personnum;
    }

    public void setPersonnum(String personnum) {
        this.personnum = personnum;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getUmobile() {
        return umobile;
    }

    public void setUmobile(String umobile) {
        this.umobile = umobile;
    }

    public String getUdepart() {
        return udepart;
    }

    public void setUdepart(String udepart) {
        this.udepart = udepart;
    }

    public String getUmajor() {
        return umajor;
    }

    public void setUmajor(String umajor) {
        this.umajor = umajor;
    }

    public String getUbatch() {
        return ubatch;
    }

    public void setUbatch(String ubatch) {
        this.ubatch = ubatch;
    }

}
