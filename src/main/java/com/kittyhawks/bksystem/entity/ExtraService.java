package com.kittyhawks.bksystem.entity;

public class ExtraService {
    private Integer id;

    private String name;

    private String size;

    private Integer status;

    private Integer amount;

    private Long price;

    private String pic;

    private Byte extraType;

    private Integer damageAmount;

    private Integer cleaningAmount;

    private Integer company;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size == null ? null : size.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic == null ? null : pic.trim();
    }

    public Byte getExtraType() {
        return extraType;
    }

    public void setExtraType(Byte extraType) {
        this.extraType = extraType;
    }

    public Integer getDamageAmount() {
        return damageAmount;
    }

    public void setDamageAmount(Integer damageAmount) {
        this.damageAmount = damageAmount;
    }

    public Integer getCleaningAmount() {
        return cleaningAmount;
    }

    public void setCleaningAmount(Integer cleaningAmount) {
        this.cleaningAmount = cleaningAmount;
    }

    public Integer getCompany() {
        return company;
    }

    public void setCompany(Integer company) {
        this.company = company;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        ExtraService other = (ExtraService) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getSize() == null ? other.getSize() == null : this.getSize().equals(other.getSize()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getAmount() == null ? other.getAmount() == null : this.getAmount().equals(other.getAmount()))
            && (this.getPrice() == null ? other.getPrice() == null : this.getPrice().equals(other.getPrice()))
            && (this.getPic() == null ? other.getPic() == null : this.getPic().equals(other.getPic()))
            && (this.getExtraType() == null ? other.getExtraType() == null : this.getExtraType().equals(other.getExtraType()))
            && (this.getDamageAmount() == null ? other.getDamageAmount() == null : this.getDamageAmount().equals(other.getDamageAmount()))
            && (this.getCleaningAmount() == null ? other.getCleaningAmount() == null : this.getCleaningAmount().equals(other.getCleaningAmount()))
            && (this.getCompany() == null ? other.getCompany() == null : this.getCompany().equals(other.getCompany()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getSize() == null) ? 0 : getSize().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getAmount() == null) ? 0 : getAmount().hashCode());
        result = prime * result + ((getPrice() == null) ? 0 : getPrice().hashCode());
        result = prime * result + ((getPic() == null) ? 0 : getPic().hashCode());
        result = prime * result + ((getExtraType() == null) ? 0 : getExtraType().hashCode());
        result = prime * result + ((getDamageAmount() == null) ? 0 : getDamageAmount().hashCode());
        result = prime * result + ((getCleaningAmount() == null) ? 0 : getCleaningAmount().hashCode());
        result = prime * result + ((getCompany() == null) ? 0 : getCompany().hashCode());
        return result;
    }
}