package com.kittyhawks.bksystem.entity;

import java.util.Date;

public class PhotograRota {
    private Integer id;

    private String pgrid;

    private Date bookDay;

    private int bookTime;

    private Byte acceptStatus;

    private Byte status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPgrid() {
        return pgrid;
    }

    public void setPgrid(String pgrid) {
        this.pgrid = pgrid == null ? null : pgrid.trim();
    }

    public Date getBookDay() {
        return bookDay;
    }

    public void setBookDay(Date bookDay) {
        this.bookDay = bookDay;
    }

    public int getBookTime() {
        return bookTime;
    }

    public void setBookTime(int bookTime) {
        this.bookTime = bookTime;
    }

    public Byte getAcceptStatus() {
        return acceptStatus;
    }

    public void setAcceptStatus(Byte acceptStatus) {
        this.acceptStatus = acceptStatus;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

}