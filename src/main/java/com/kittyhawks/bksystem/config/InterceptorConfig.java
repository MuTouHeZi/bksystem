package com.kittyhawks.bksystem.config;

import com.kittyhawks.bksystem.Interceptor.BackManagerLoginInterceptor;
import com.kittyhawks.bksystem.weixin.interceptor.WeixinLoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

/**
 * @Author: liangbin
 * @Date: 2018/3/25 14:09
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {
    @Autowired
    BackManagerLoginInterceptor backManagerLoginInterceptor;

    @Autowired
    WeixinLoginInterceptor weixinLoginInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(backManagerLoginInterceptor).addPathPatterns("/bksystem/**").excludePathPatterns("/bksystem/backsyslogin");
        registry.addInterceptor(weixinLoginInterceptor).addPathPatterns("/static/wx/**").addPathPatterns("/order/**").addPathPatterns("/user/**");

    }
}
